const BASEURL = "https://jobinja.ir/jobs";
const SalMins = ['', '2655000', '3000000', '4000000', '5000000', '7000000', '8000000', '10000000', '12000000', '15000000', '20000000', '35000000', '50000000']
const WEs = ['any', 'under_two', '3_6', '7_plus']
const JobTypes = ['is_fulltime', 'is_parttime'];
const Types = ['[internship]=1', 'filters[remote]=1']
const Positions = ['کارمند', 'کارشناس', 'کارشناس ارشد', 'سرپرست', 'مدیر', 'معاونت']
var result = { 1: {}, 2: {} }
var dataTitle = [
    'کارمند/کارشناس/کارشناس ارشد',
    'سرپرست / مدیر / معاونت'
]
var counterLog = total = progress = positionIndex = 0;
var cities = categories = [];

const getPositionIndex = (i) => {
    let positionTitle;
    if (i < 3) {
        positionTitle = 1;
    } else if (i > 2) {
        positionTitle = 2;
    }
    return positionTitle;
}
const toEnglishDigits = (str) => {
    let e = '۰'.charCodeAt(0);
    str = str.replace(/[۰-۹]/g, function(t) {
        return t.charCodeAt(0) - e;
    });
    e = '٠'.charCodeAt(0);
    str = str.replace(/[٠-٩]/g, function(t) {
        return t.charCodeAt(0) - e;
    });
    return str;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const getJobOfferCount = () => {
    let value = $('.c-jobSearchState__numberOfResultsEcho')[0].innerText;
    let res = value.replace(',', '').split(' ')[0]
    return parseInt(toEnglishDigits(res));
}

function percentage(partialValue, totalValue) {
    let res = (100 * partialValue) / totalValue;
    return Math.round((res) * 100) / 100;
}



// 1: city 2: category
const getPrepareData = (type = 1) => {
    let data = [];
    let value = $('.select2able')[type - 1].innerText;
    value.trim().split('\n').map(function(e) {
        data.push(e.trim())
        first = true;
    })
    return data;
}

// let url = `${BASEURL}?filters[job_categories][]=${category}&filters[keywords][0]=${position}&filters[locations][]=${city}&preferred_before=${time}&filters[job_types][1]=${job_type}&filters[w_e][]=${experience}&filters[sal_min][2]=${sal_min}:&sort_by=${sort}`;
const getJobsData = (city = '', category = '', sort = 'relevance_desc', job_type = '', experience = '', sal_min = '') => {
    let index = getPositionIndex(positionIndex);
    let time = Math.floor(Date.now() / 1000);
    city = (cities.indexOf(city) != 0) ? city : '';
    category = (categories.indexOf(category) != 0) ? category : '';
    let url = `${BASEURL}?filters[locations][]=${city}&preferred_before=${time}&filters[job_categories][]=${category}&filters[keywords][0]=${Positions[positionIndex]}&sort_by=relevance_desc`;

    if (result[index][city] === undefined) {
        result[index][city] = {}
    }
    if (result[index][city][category] === undefined) {
        result[index][city][category] = 0;
    }
    $.ajax({
        type: "GET",
        url: url,
        success: data => {
            let html = data.substring(
                data.indexOf(' <header class="c-header js-header  is-notLoggedIn">') + 1,
                data.lastIndexOf(' <script type="text/javascript">')
            )
            $('#jobinja').html(html.replace('src', '').replace('href', ''));

            result[index][city][category] += getJobOfferCount()
                ++counterLog;
            progress++;
            let percent = percentage(progress, cities.length * categories.length * (Positions.length - 1));
            $('#progress').text(`${percent} %`).css('width', `${percent}%`)
            if (counterLog == cities.length * categories.length) {
                console.log(`${Positions[positionIndex]} - completed`)
                counterLog = 0;
                if (positionIndex != Positions.length - 1) {
                    console.log(positionIndex, ' i= ', Positions.length - 1)
                    positionIndex++;
                    setTimeout(loopJobs(positionIndex), 50000)
                    console.log('start new')
                } else {
                    console.log('completed')
                }
            }
        },
        error: e => {
            console.error(e.msg);
        }
    })
}
const loopJobs = () => {
    cities.forEach((city, index) => {
        setTimeout(function() {
            categories.forEach(category => {
                getJobsData(city, category)
            })
        }, index * 1000);
    })
}
const categoryFunc = (city, categories) => {
    categories.forEach(category => {

        if (result[positionTitle][city] === undefined) {
            result[positionTitle][city] = {}
        }

        if (result[positionTitle][city][category] === undefined) {
            result[positionTitle][city][category] = 0;
        }
        getJobsData(city, category, Positions[positionIndex]);
    })
}
$(function() {
    let time = Math.floor(Date.now() / 1000);
    $.ajax({
        type: "GET",
        url: `${BASEURL}?preferred_before=${time}`,
        success: data => {
            let html = data.substring(
                data.indexOf(' <header class="c-header js-header  is-notLoggedIn">') + 1,
                data.lastIndexOf(' <script type="text/javascript">')
            )
            $('#jobinja').html(html.replace('src', '').replace('href', ''));

            cities = getPrepareData(1)
            categories = getPrepareData(2)
            total = cities.length * categories.length * Positions.length;
            $('#progress').text('0 %').css('width', '0%')
            loopJobs();
            console.log(result)
        },
        error: e => {
            console.error(e.msg);
        }
    })
});